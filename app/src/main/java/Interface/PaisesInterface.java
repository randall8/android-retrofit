package Interface;

import java.util.List;

import models.Casos;
import models.Pais;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PaisesInterface {
    @GET("country/{pais}/status/confirmed")
    Call<List<Casos>> getCasos(@Path("pais") String pais, @Query("from") String yesterday, @Query("to") String today);

    @GET("countries")
    Call<List<Pais>> getPaises();
}
