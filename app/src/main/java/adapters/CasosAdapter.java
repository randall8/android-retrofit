package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cr.ac.ucr.labandroid.R;
import models.Casos;

public class CasosAdapter extends RecyclerView.Adapter<CasosAdapter.CasosViewHolder> {

    private List<Casos>lista;
    private Context context;

    public CasosAdapter(List<Casos> lista, Context context) {
        this.lista = lista;
        this.context = context;
    }

    @NonNull
    @Override
    public CasosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View listItem = inflater.inflate(R.layout.casos_items, parent, false);
        CasosViewHolder viewHolder = new CasosViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CasosViewHolder holder, int position) {
        final Casos casos = lista.get(position);
        holder.tw_cod_pais.setText(casos.getCountryCode());
        holder.tw_casos_pais.setText("Casos: "+casos.getCases());
        holder.tw_estado_pais.setText("Estado: "+casos.getProvince());
        if(casos.getProvince().equals("")){
            holder.tw_estado_pais.setText("Estado: -");
        }
        holder.tw_fecha_pais.setText("Fecha: "+casos.getDate());
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public static class CasosViewHolder extends RecyclerView.ViewHolder{

        public TextView tw_cod_pais;
        public TextView tw_casos_pais;
        public TextView tw_estado_pais;
        public TextView tw_fecha_pais;
        public ConstraintLayout cl_casos_items;

        public CasosViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tw_cod_pais = (TextView) itemView.findViewById(R.id.tw_cod_pais);
            this.tw_casos_pais = (TextView) itemView.findViewById(R.id.tw_casos_pais);
            this.tw_estado_pais = (TextView) itemView.findViewById(R.id.tw_estado_pais);
            this.tw_fecha_pais = (TextView) itemView.findViewById(R.id.tw_fecha_pais);
            this.cl_casos_items = (ConstraintLayout) itemView.findViewById(R.id.cl_casos_items);
        }
    }
}
