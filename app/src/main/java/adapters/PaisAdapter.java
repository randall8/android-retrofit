package adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cr.ac.ucr.labandroid.CasosActivity;
import cr.ac.ucr.labandroid.R;
import models.Pais;

public class PaisAdapter extends RecyclerView.Adapter<PaisAdapter.PaisViewHolder> {

    private List<Pais> lista;
    private Context context;

    public PaisAdapter(List<Pais> lista, Context context) {
        this.lista = lista;
        this.context = context;
    }

    @NonNull
    @Override
    public PaisViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View listItem = inflater.inflate(R.layout.paises_items, parent, false);
        PaisViewHolder viewHolder = new PaisViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PaisViewHolder holder, int position) {
        final Pais pais = lista.get(position);
        holder.tw_paises.setText(pais.getCountry());

        holder.cl_paises_items.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CasosActivity.class);
                intent.putExtra("selected",pais.getCountry());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public static class PaisViewHolder extends  RecyclerView.ViewHolder{

        public TextView tw_paises;
        public ConstraintLayout cl_paises_items;

        public PaisViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tw_paises = (TextView) itemView.findViewById(R.id.tw_pais);
            this.cl_paises_items = (ConstraintLayout) itemView.findViewById(R.id.cl_paises_items);
        }

    }

}
