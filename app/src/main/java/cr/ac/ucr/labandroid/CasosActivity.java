package cr.ac.ucr.labandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import Interface.PaisesInterface;
import adapters.CasosAdapter;
import models.Casos;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CasosActivity extends AppCompatActivity implements Callback<List<Casos>> {
    private RecyclerView recycler_casos;
    private TextView tw_caso;
    private List<Casos> lista;
    private String seleccionado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_casos);

        recycler_casos = findViewById(R.id.recycler_casos);
        seleccionado = getIntent().getStringExtra("selected");
        tw_caso = (TextView) findViewById(R.id.tw_caso);
        tw_caso.setText(seleccionado);
        irCasos(seleccionado);
    }

    public void getCasos(String pais, String yesterday, String today){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.covid19api.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PaisesInterface p = retrofit.create(PaisesInterface.class);
        Call<List<Casos>> call = p.getCasos(pais, yesterday, today);

        call.enqueue(this);
    }

    public void irCasos(String pais){
        Date f1 = Calendar.getInstance().getTime();
        String today = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(f1);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -1);
        Date f2 = c.getTime();
        String yesterday = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(f2);
        getCasos(pais,yesterday.toString()+"T00:00:00Z",today.toString()+"T00:00:00Z");
    }

    @Override
    public void onResponse(Call<List<Casos>> call, Response<List<Casos>> response) {
        if(response.isSuccessful()){
            lista = response.body();
            CasosAdapter adapter = new CasosAdapter(lista, this);
            recycler_casos.setLayoutManager(new LinearLayoutManager(this));
            recycler_casos.setAdapter(adapter);
        }
    }

    @Override
    public void onFailure(Call<List<Casos>> call, Throwable t) {

    }
}
