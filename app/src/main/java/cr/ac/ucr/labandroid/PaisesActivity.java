package cr.ac.ucr.labandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import Interface.PaisesInterface;
import adapters.PaisAdapter;
import models.Pais;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PaisesActivity extends AppCompatActivity implements Callback<List<Pais>> {
    private RecyclerView recycler_paises;
    private List<Pais>lista = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paises);

        recycler_paises = findViewById(R.id.recycler_paises);
        getPaises();

    }

    public void getPaises(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.covid19api.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PaisesInterface p = retrofit.create(PaisesInterface.class);
        Call<List<Pais>> call = p.getPaises();

        call.enqueue(this);

    }

    @Override
    public void onResponse(Call<List<Pais>> call, Response<List<Pais>> response) {
        if(response.isSuccessful()){
            lista = response.body();
            PaisAdapter adapter = new PaisAdapter(lista,this);
            recycler_paises.setLayoutManager(new LinearLayoutManager(this));
            recycler_paises.setAdapter(adapter);
        }
    }

    @Override
    public void onFailure(Call<List<Pais>> call, Throwable t) {

    }
}
