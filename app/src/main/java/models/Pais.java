package models;

import java.util.HashMap;
import java.util.Map;

public class Pais {
    private String Country;
    private String Slug;
    private String ISO2;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        this.Country = country;
    }

    public String getSlug() {
        return Slug;
    }

    public void setSlug(String slug) {
        this.Slug = slug;
    }

    public String getISO2() {
        return ISO2;
    }

    public void setISO2(String iSO2) {
        this.ISO2 = iSO2;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
